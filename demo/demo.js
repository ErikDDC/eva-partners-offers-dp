import '@bbva-web-components/bbva-foundations-theme/bbva-foundations-theme.js';
import './css/demo-styles.js';
import '../eva-partners-offers-dp.js';

// Include below here your components only for demo
// import 'other-component.js'
import '@bbva-web-components/bbva-core-demo-dm-helper/bbva-core-demo-dm-helper';
import '@bbva-web-components/bbva-button-default/bbva-button-default';
import '@bbva-web-components/bbva-web-form-text/bbva-web-form-text';
