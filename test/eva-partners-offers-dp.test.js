/* eslint-disable import/no-extraneous-dependencies */
import { html, fixture, assert, fixtureCleanup, oneEvent } from '@open-wc/testing';
import { stub } from 'sinon';
import '../eva-partners-offers-dp.js';

suite('EvaPartnersOffersDp', () => {
  let el;

  teardown(() => fixtureCleanup());

  setup(async () => {
    el = await fixture(html`<eva-partners-offers-dp
      host ="https://int-arqaso.work.mx.nextgen.igrupobbva:8050"
      partner-id="app.mx.pyme002"
    ></eva-partners-offers-dp>`);
    await el.updateComplete;
  });

  test('unit test properties', () => {
    assert.equal(el.host, 'https://int-arqaso.work.mx.nextgen.igrupobbva:8050');
    assert.equal(el.partnerId, 'app.mx.pyme002');
  });

  test('partners-offers-dp-success event test', async() => {
    const request = stub(el.genericDp, 'generateRequest').resolves({ status: 200, data: true });
    const listener = oneEvent(el, 'partners-offers-dp-success');

    el.getPartnersOffers();

    const { detail } = await listener;
    assert.deepEqual(detail, {status: 200, data: true});
    assert.isTrue(detail);
    assert.strictEqual(request.calledOnce)
  });

  test('partners-offers-dp-error event test', async() => {
    const request = stub(el.genericDp, 'generateRequest').resolves({ status: 200, data: true });
    const listener = oneEvent(el, 'partners-offers-dp-error');

    el.getPartnersOffers();

    const { detail } = await listener;

    assert.deepEqual(detail, {status: 204, data: true});
    assert.strictEqual(request.calledOnce)
  });
});
